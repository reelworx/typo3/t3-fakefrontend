<?php

declare(strict_types=1);

namespace Reelworx\TYPO3\FakeFrontend;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Throwable;
use TYPO3\CMS\Core\Authentication\AbstractUserAuthentication;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\TypoScriptAspect;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\ExtbaseRequestParameters;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class FakeFrontendService
{
    public ?Throwable $lastError = null;

    /* backups */
    protected ?ServerRequest $request = null;
    protected ?AbstractUserAuthentication $beUser = null;

    public function executeInFEContext(int $pageUid, \Closure $work): void
    {
        $this->executeInFEContextWithLanguage($pageUid, 0, $work);
    }

    public function executeInFEContextWithLanguage(int $pageUid, int $languageId, \Closure $work): void
    {
        $valid = $this->buildFakeFE($pageUid, $languageId);
        if (!$valid) {
            // resetGlobals is done internally already
            throw new RuntimeException(
                'Building fake FE failed for page uid: ' . $pageUid,
                0,
                $this->lastError
            );
        }
        $work();
        $this->resetGlobals();
    }

    /**
     * Initialize Core's global variables to simulate a frontend request to $pageUid
     *
     * @param int $pageUid The page the request should be created for (preferably a root page)
     * @param int|null $languageId Select specific language by id defined in site configuration (0 = use default)
     * @return bool
     */
    public function buildFakeFE(int $pageUid, ?int $languageId = 0): bool
    {
        if (!$pageUid) {
            throw new InvalidArgumentException('You must specify a page id.');
        }

        // do not touch anything if we have a TSFE already (wherever it may come from)
        if (isset($GLOBALS['TSFE'])) {
            return true;
        }

        // remove the current PageRenderer singleton (it may be one from the backend context)
        /** @var PageRenderer $pageRendererBackup */
        $pageRendererBackup = GeneralUtility::makeInstance(PageRenderer::class);
        $instances = GeneralUtility::getSingletonInstances();
        unset($instances[PageRenderer::class]);
        GeneralUtility::resetSingletonInstances($instances);

        $this->request = $GLOBALS['TYPO3_REQUEST'] ?? null;
        $this->beUser = $GLOBALS['BE_USER'] ?? null;
        $validTSFE = false;
        try {
            /** @var Context $context */
            $context = GeneralUtility::makeInstance(Context::class);
            $site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($pageUid);
            $siteLanguage = $languageId ? $site->getLanguageById($languageId) : $site->getDefaultLanguage();
            $pageArgs = new PageArguments($pageUid, '0', []);

            /** @var TypoScriptAspect $aspect */
            $aspect = GeneralUtility::makeInstance(TypoScriptAspect::class, true);
            $context->setAspect('typoscript', $aspect);

            // simulate a normal FE without any logged-in FE or BE user
            $uri = $site->getBase();
            $request = new ServerRequest(
                $uri,
                'GET',
                'php://input',
                [],
                [
                    'HTTP_HOST' => $uri->getHost(),
                    'SERVER_NAME' => $uri->getHost(),
                    'HTTPS' => $uri->getScheme() === 'https',
                    'SCRIPT_FILENAME' => __FILE__,
                    'SCRIPT_NAME' => rtrim($uri->getPath(), '/') . '/',
                ]
            );

            // needed by Extbase UriBuilder to really believe it's a frontend request
            $GLOBALS['TYPO3_REQUEST'] = $request
                ->withAttribute('applicationType', SystemEnvironmentBuilder::REQUESTTYPE_FE)
                ->withAttribute('site', $site)
                ->withAttribute('fakefe', true)
                ->withAttribute('extbase', new ExtbaseRequestParameters());

            // some link generation relies on HTTP_HOST, so make sure we simulate the HTTP_HOST that matches our request
            $_SERVER['HTTP_HOST'] = $uri->getHost();

            /** @var FrontendUserAuthentication $frontendUser */
            $frontendUser = GeneralUtility::makeInstance(FrontendUserAuthentication::class);
            $frontendUser->start($GLOBALS['TYPO3_REQUEST']);
            $this->unpack_uc($frontendUser);

            /** @var TypoScriptFrontendController $tsfe */
            $tsfe = GeneralUtility::makeInstance(
                TypoScriptFrontendController::class,
                $context,
                $site,
                $siteLanguage,
                $pageArgs,
                $frontendUser
            );
            $GLOBALS['TSFE'] = $tsfe;

            $tsfe->determineId($request);
            $tsfe->getFromCache($request);
            // Core v11 only
            if (method_exists($tsfe, 'getConfigArray')) {
                $tsfe->getConfigArray();
            }
            $tsfe->newCObj();

            $validTSFE = true;
        } catch (Throwable $e) {
            $this->lastError = $e;
            $this->resetGlobals();
        }

        // we got our TSFE up and running, restore the PageRenderer
        GeneralUtility::setSingletonInstance(PageRenderer::class, $pageRendererBackup);

        if ($validTSFE) {
            // calculate the absolute path prefix
            if (!empty($GLOBALS['TSFE']->config['config']['cliDomain'])) {
                $absRefPrefix = trim($GLOBALS['TSFE']->config['config']['cliDomain']);
                if ($absRefPrefix === 'auto') {
                    $GLOBALS['TSFE']->absRefPrefix = GeneralUtility::getIndpEnv('TYPO3_SITE_PATH');
                } else {
                    $GLOBALS['TSFE']->absRefPrefix = $absRefPrefix;
                }
            } else {
                $GLOBALS['TSFE']->absRefPrefix = '';
            }
        }

        return $validTSFE;
    }

    public function resetGlobals(): void
    {
        unset($GLOBALS['TSFE']);
        if ($this->request) {
            $GLOBALS['TYPO3_REQUEST'] = $this->request;
        }
        if ($this->beUser) {
            $GLOBALS['BE_USER'] = $this->beUser;
        }
    }

    /**
     * The built UriBuilder behaves as if it was in FE mode
     */
    public function getFrontendUriBuilder(): UriBuilder
    {
        /** @var ServerRequestInterface $globalRequest */
        $globalRequest = $GLOBALS['TYPO3_REQUEST'];
        if (!$globalRequest->getAttribute('fakefe', false)) {
            throw new InvalidArgumentException('The global TYPO3_REQUEST must be initialized by FrontendUtility::buildFakeFE. Consider using FrontendUtility::executeInFEContext for simplicity.');
        }
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        $uriBuilder->setRequest(new Request($globalRequest));
        return $uriBuilder;
    }

    /**
     * Copy from the core as this is not accessible in v12 anymore
     */
    protected function unpack_uc(AbstractUserAuthentication $tsfe): void
    {
        if (isset($tsfe->user['uc'])) {
            $theUC = unserialize($tsfe->user['uc'], ['allowed_classes' => false]);
            if (is_array($theUC)) {
                $tsfe->uc = $theUC;
            }
        }
    }
}
