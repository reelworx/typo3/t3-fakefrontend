#!/usr/bin/env bash

if [[ -x $(which "podman-compose") ]]; then
    composecommand="podman-compose"
else
    composecommand="podman compose"
fi

mkdir -p .Build/phpcs
$composecommand run --rm web composer req --working-dir=.Build/phpcs --dev -W friendsofphp/php-cs-fixer
$composecommand run --rm web .Build/phpcs/vendor/bin/php-cs-fixer fix -v --diff "$@"
result=$?
$composecommand down
exit $result
