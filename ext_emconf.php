<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Fake Frontend library for extensions',
    'description' => 'A library for TYPO3 extensions to initialize a "fake frontend" environment. Mainly for CLI/BE purposes.',
    'category' => '',
    'author' => 'Markus Klein',
    'author_email' => 'support@reelworx.at',
    'author_company' => 'Reelworx GmbH',
    'state' => 'stable',
    'uploadfolder' => 0,
    'version' => '3.1.0',
    'constraints' => [
        'depends' => [
            'php' => '8.1.0-8.4.99',
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
