# Change Log

## 3.1.0

* The library is now a real TYPO3 extension too
* Dependency Injection is now supported
* Deprecated FrontendUtility
* Added option to use specific language for fake frontend

## 3.0.1

* Fixed Extbase request creation

## 3.0.0

* Dropped TYPO3 v10 support
* Added TYPO3 v12 support
